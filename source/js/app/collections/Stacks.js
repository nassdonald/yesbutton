define([ 'jquery', 'marionette', 'velocity', 'lazyload', 'config/vent', 'views/FooterView', 'text!templates/projects/base-project.html', 'text!templates/home.html', 'text!templates/projects/salesseek.html', 'text!templates/projects/phonicsschool.html', 'text!templates/projects/picturehouse.html', 'text!templates/projects/rbmusic.html', 'text!templates/projects/okbutton.html' ],
    function( $, Marionette, Velocity, lazyload, vent, FooterView, baseProjectTemplate, homeTemplate, salesSeekTemplate, phonicsSchoolTemplate, pictureHouseTemplate, rbmusicTemplate, okbuttonTemplate ) {

        var BaseContentView = Marionette.LayoutView.extend({
            className: function() {
                var projectClasses = '';
                if (this.model.get('isProject')) {
                    projectClasses = ' project project-' + this.model.id;
                }
                return 'content' + projectClasses;
            },
            template: _.template(baseProjectTemplate),
            templateHelpers: function() {
                return {
                    inverseHero: this.model.get('inverse') ? 'inverse' : ''
                };
            },
            regions: {
                footer: '.footer'
            },
            events: {
                'click .scroll-cta': function() {
                    this.$el.find('.stack-block:first-child').velocity('scroll');
                },
                'click [data-slide]': function(ev) {
                    var slideTrigger = $(ev.currentTarget),
                        activeSlide = this.$el.find('#' + slideTrigger.data('slide'));
                    this.showActiveSlide(activeSlide, slideTrigger);
                    clearInterval(this.slideshows[activeSlide.closest('.slideshow').attr('id')]);
                },
                'click .slideshow img': function(ev) {
                    var slide = $(ev.currentTarget),
                        slideshow = slide.closest('.slideshow'),
                        next = slide.parent('li').next('li')

                    if (slide.parent('li').is(':last-child')) {
                        next = slideshow.find('li:first-child');
                    }

                    this.showActiveSlide(next);
                    clearInterval(this.slideshows[slideshow.attr('id')]);
                }
            },
            modelEvents: {
                'change:active': 'onModelActive',
                'change:hero-height': 'resizeHero'
            },
            onModelActive: function(model, active) {
                if (active || active === undefined) {
                    this.lazyloadImages();
                }
            },
            onShow: function() {
                var cta = this.$el.find('.scroll-cta');

                if (this.model.get('active')) {
                    this.onModelActive();
                }

                if (this.model.get('isProject')) {
                    this.showChildView('footer', new FooterView());
                }

                if (this.model.get('slideshows')) {
                    this.autoPlaySlides();
                }

                $(window).on('scroll.' + this.model.id, function() {
                    cta.toggleClass('invisible', $(this).scrollTop() > 400);
                });
            },
            onBeforeDestroy: function() {
                $(window).off('scroll.' + this.model.id);
            },
            resizeHero: function(model, height) {
                var inner = this.$el.find('.hero-inner'),
                    hero = this.$el.find('.hero-unit'),
                    difference = Math.floor( (height - inner.height())/2 - 2 ),
                    padding = inner.innerHeight() > height ? difference : 0,
                    minPadding = inner.innerHeight() > height ? 80 : 144;

                if (this.model.get('height') !== height) {
                    hero.css('minHeight', height);
                }

                if (inner.is(':visible')) {
                    inner.css({
                        paddingTop: Math.max( difference, minPadding) // Set min padding
                    });
                }
            },
            lazyloadImages: function() {
                this.$el.find('.lazy').not('.loaded, .custom-lazy')
                    .lazyload({
                        threshold: -100,
                        effect: 'fadeIn',
                        load: function() {
                            $(this).addClass('loaded');
                        }
                    });
            },
            autoPlaySlides: function() {
                var that = this;

                this.$el.find('.slideshow .lazy').not('.loaded').lazyload({
                    event: 'lazyload-slide',
                    load: function() {
                        $(this).addClass('loaded');
                    }
                });

                this.slideshows = {};

                _.each(this.model.get('slideshows'), function(slideshow) {
                    that.showActiveSlide(that.$el.find('#' + slideshow.prefix + '-1'));
                    that.slideshows[slideshow.id] = autoPlay('#' + slideshow.prefix + '-', slideshow.total);
                });

                function autoPlay(prefix, totalSlides) {
                    var counter = 1;
                    return setInterval(function() {
                        counter++;

                        that.showActiveSlide(that.$el.find(prefix + counter));

                        if (counter === totalSlides) {
                            counter = 0;
                        }
                    }, 4000);
                }
            },
            showActiveSlide: function(slide, trigger) {
                var slideTrigger = trigger || this.$el.find('[data-slide=' + slide.attr('id') + ']'),
                    slideTriggers = this.$el.find('[data-slideshow=' + slide.closest('.slideshow').attr('id') + ']');

                if (slideTrigger) {
                    slideTriggers.find('.active').not(slideTrigger).removeClass('active');
                    slideTrigger.addClass('active');
                }

                slide.siblings('.active')
                    .removeClass('active')
                    .velocity('stop')
                    .velocity({
                        opacity: 0
                    }, {
                        visibility: 'hidden',
                        duration: 400
                    });

                if (!slide.hasClass('active')) {
                    slide.find('.lazy').not('.loaded').trigger('lazyload-slide');
                    slide
                        .addClass('active')
                        .velocity('stop')
                        .velocity({
                            opacity: 1
                        }, {
                            visibility: 'visible',
                            duration: 400
                        });
                }
            }
        })

        var HomeView = BaseContentView.extend({
            template: _.template(homeTemplate),
            ui: {
                introStart: '.intro-start',
                introEnd: '.intro-end',
                logo: '.logo',
                yes: '.yes'
            },
            events: {
                'keydown @ui.logo': function(ev) {
                    // Enter key
                    if (ev.which === 13) {
                        ev.preventDefault();
                        this.onIntroStart();
                    }
                },
                'click @ui.yes': function(ev) {
                    ev.preventDefault();
                    this.onIntroStart();
                },
                'click .cta-button': function(ev) {
                    $(ev.currentTarget).removeClass('pulse');
                    vent.trigger('projects:show');
                }
            },
            onModelActive: function(model, active) {
                if (!this.introStarted && !active) {
                    this.onIntroStart(true);
                }

                // Super invocation!
                BaseContentView.prototype.onModelActive.apply(this, arguments);
            },
            onShow: function() {
                if (this.model.get('active')) {
                    this.showIntro();
                } else {
                    vent.trigger('intro:show', false);
                    this.introStarted = true;
                }

                this.listenTo(vent, 'intro:reset', this.showIntro);
            },
            onDestroy: function() {
                vent.trigger('intro:show', false);
            },
            showIntro: function() {
                if (this.introStarted) {
                    this.ui.introStart
                        .removeAttr('style')
                        .removeClass('intro-started');
                    this.ui.introEnd.removeAttr('style');
                    this.introStarted = false;
                }

                vent.trigger('intro:show', true);

                this.ui.logo
                    .addClass('intro-position')
                    .removeAttr('tabindex');
            },
            onIntroStart: function(immediate) {
                if (this.introStarted) {
                    return;
                }

                var that = this;
                var logo = this.getLogoSizes();
                var introEnd = this.ui.introEnd;
                var duration = immediate ? 0 : 700;
                var easing = [200, 20];

                this.introStarted = true;

                this.ui.introStart
                    .addClass('intro-started')
                    .velocity({
                        left: logo.left,
                        width: logo.width,
                        height: logo.height
                    }, {
                        duration: duration,
                        easing: easing
                    })
                    .velocity('fadeOut', {
                        duration: duration,
                        easing: 'easeOut',
                        queue: false,
                        delay: duration/3
                    });

                this.ui.logo
                    .velocity({
                        top: 0,
                        marginTop: 0,
                        marginLeft: logo.marginLeft
                    }, {
                        duration: duration,
                        easing: easing,
                        complete: function() {
                            $(this)
                                .removeClass('intro-position')
                                .attr('tabindex', '-1');

                            $.Velocity.hook(introEnd, 'visibility', 'visible');

                            introEnd.velocity({
                                opacity: 1,
                            }, {
                                duration: duration,
                                delay: duration/2,
                                complete: function() {
                                    vent.trigger('intro:show', false);
                                }
                            });

                            $.Velocity.hook($(this), 'top', '');
                            $.Velocity.hook($(this), 'marginTop', '');
                            $.Velocity.hook($(this), 'marginLeft', '');
                        }
                    });
            },
            getLogoSizes: function() {
                var logo = this.ui.logo,
                    logoSizes = {};

                logo.removeClass('intro-position');

                logoSizes.left = logo.offset().left;
                logoSizes.width = logo.outerWidth();
                logoSizes.height = logo.find('.logo-top').outerHeight();
                logoSizes.marginLeft = logo.css('marginLeft');

                logo.addClass('intro-position');

                return logoSizes;
            }
        });

        var SalesSeekView = BaseContentView.extend({
            template: _.template(salesSeekTemplate),
            events: _.extend({
            }, BaseContentView.prototype.events),
            onShow: function() {

                // Super invocation!
                BaseContentView.prototype.onShow.apply(this, arguments);
            }
        });

        var PhonicsSchoolView = BaseContentView.extend({
            // template: _.template(phonicsSchoolTemplate)
        });

        var PicturehouseView = BaseContentView.extend({
            template: _.template(pictureHouseTemplate)
        });

        var RBMusicView = BaseContentView.extend({
            template: _.template(rbmusicTemplate)
        });

        var OKButtonView = BaseContentView.extend({
            // template: _.template(okbuttonTemplate)
        });

        return [
            {
                isProject: false,
                id: 'home',
                route: '',
                name: 'Home',
                description: '',
                logo: '',
                inverse: true,
                detailView: HomeView
            }, {
                isProject: true,
                id: 'salesseek',
                route: 'projects/salesseek',
                name: 'SalesSeek',
                description: 'A collection of UI, UX, front-end and graphic design work for an ambitious London-based startup. Web app, website, mobile app – the whole package!',
                logo: 'img-src/salesseek/ss-logo-white.png',
                background: 'img-src/salesseek/ss-bg.jpg',
                inverse: true,
                detailView: SalesSeekView,
                slideshows: [{
                    id: 'ss-iphone-slideshow',
                    prefix: 'ss-iphone',
                    total: 6
                }, {
                    id: 'ss-dashboards-slideshow',
                    prefix: 'ss-dashboards',
                    total: 3
                }, {
                    id: 'ss-datavis-slideshow',
                    prefix: 'ss-datavis',
                    total: 4
                }, {
                    id: 'ss-campaign-slideshow',
                    prefix: 'ss-campaign',
                    total: 4
                }, {
                    id: 'ss-pipeline-slideshow',
                    prefix: 'ss-pipeline',
                    total: 2
                }]
            }, {
            //     isProject: true,
            //     id: 'phonicsschool',
            //     route: 'projects/phonicsschool',
            //     name: 'Phonics School',
            //     description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            //     logo: 'img-src/phonicsschool-bg.png',
            //     inverse: false,
            //     detailView: PhonicsSchoolView
            // }, {
                isProject: true,
                id: 'rbmusic',
                route: 'projects/rbmusic',
                name: 'R&B Music',
                description: 'A series of graphics and promotional material created for an established music retail store.',
                logo: '/img-src/rbmusic/rbmusic-logo-white.png',
                background: 'img-src/rbmusic/rbmusic-guild-pip-photo2.jpg',
                inverse: true,
                detailView: RBMusicView
            }, {
                isProject: true,
                id: 'picturehouse',
                route: 'projects/picturehouse',
                name: 'Picturehouse',
                description: 'A commission to design new promotional material for a popular weekly film quiz event.',
                logo: 'img-src/picturehouse/ph-logo-black.png',
                background: 'img-src/picturehouse/ph-bg.jpg',
                inverse: false,
                detailView: PicturehouseView
            // }, {
            //     isProject: true,
            //     id: 'okbutton',
            //     route: 'projects/okbutton',
            //     name: 'OK Button',
            //     description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            //     logo: 'img-src/okbutton-bg.png',
            //     inverse: false,
            //     detailView: OKButtonView
            }
        ];
    });