define(['backbone', 'backbone.wreqr'],
    function (Backbone, Wreqr) {
    return new Backbone.Wreqr.EventAggregator();
});