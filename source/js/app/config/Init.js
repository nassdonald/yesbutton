require.config({
    baseUrl:"js/app",
    // 3rd party script alias names (Easier to type "jquery" than "../libs/jquery/dist/jquery.min, etc")
    // probably a good idea to keep version numbers in the file names for updates checking
    paths:{
        // Core Libraries
        "jquery": "../libs/jquery-2.1.4.min",
        "underscore": "../libs/backbone.marionette/underscore-min",
        "backbone": "../libs/backbone.marionette/backbone-min",
        "marionette": "../libs/backbone.marionette/backbone.marionette.min",
        "velocity": "../libs/velocity.min",

        // Plugins
        "text": "../libs/text",
        "backbone.wreqr": "../libs/backbone.marionette/backbone.wreqr",
        "backbone.babysitter": "../libs/backbone.marionette/backbone.babysitter",
        "backbone.validation": "../libs/backbone-validation-amd-min",
        "fastclick": "../libs/fastclick.min",
        "lazyload": "../libs/jquery.lazyload.min"
    },
    // Sets the configuration for your third party scripts that are not AMD compatible
    shim:{
        "jquery": {
            exports : "jQuery"
        },
        "underscore": {
            exports: "_"
        },
        "backbone": {
            deps:["jquery", "underscore"],
            // Exports the global window.Backbone object
            exports: "Backbone"
        },
        "velocity": ["jquery"],
        "lazyload": ["jquery"]
    }
});

// Includes Desktop Specific JavaScript files here (or inside of your Desktop router)
require(["jquery", "App"],
    function ($, App) {

    $(function() {
        App.start();
    });

});