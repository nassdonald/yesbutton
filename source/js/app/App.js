define(['jquery', 'backbone', 'marionette', 'config/vent', 'routers/AppRouter', 'controllers/Controller', 'layouts/RootLayoutView'],
    function ($, Backbone, Marionette, vent, AppRouter, Controller, RootLayoutView) {

        var App = new Backbone.Marionette.Application();

        function isMobile() {
            var ua = (navigator.userAgent || navigator.vendor || window.opera, window, window.document);
            return (/iPhone|iPod|iPad|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
        }

        App.static = {};

        App.static.mobile = isMobile();

        App.on('before:start', function() {
            this.rootLayout = new RootLayoutView();
            this.rootLayout.onAppShow();
            this.appRouter = new AppRouter({
                controller: new Controller()
            });

            // Enable :active styles in mobile safari and android
            // https://css-tricks.com/snippets/css/remove-gray-highlight-when-tapping-links-in-mobile-safari/
            document.addEventListener("touchstart", function(){}, true);
        });

        App.on('start', function (options) {
            // Research pushstate: http://artsy.github.io/blog/2012/06/25/replacing-hashbang-routes-with-pushstate/
            if (Backbone.history && !Backbone.History.started) Backbone.history.start();

            $(document).on('keydown', function(ev) {
                var target = $(ev.target);

                // Return if an input/textarea has focus
                if (target.is('input') && !target.is('input[type=submit]') || target.is('textarea')) {
                    return;
                }

                // 'M' key
                if (ev.which === 77) {
                    App.rootLayout.toggleMenu();
                }
                // ESC key
                if (ev.which === 27) {
                    App.rootLayout.onEscapeKey();
                }
                // Up key
                if (ev.which === 38) {
                    if (ev.shiftKey) {
                        App.rootLayout.onStackPrev();
                    } else {
                        App.rootLayout.onUpKey(ev);
                    }
                }
                // Down key
                if (ev.which === 40) {
                    if (ev.shiftKey) {
                        App.rootLayout.onStackNext();
                    } else {
                        App.rootLayout.onDownKey(ev);
                    }
                }
            });
        });

        return App;
    });