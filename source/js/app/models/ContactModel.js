define([ 'backbone' ],
    function ( Backbone ) {

        return Backbone.Model.extend({
            defaults: {
                name: '',
                email: '',
                message: ''
            },
            validation: {
                name: {
                    required: true,
                    msg: 'Even a fake name will do, like Admiral Anonymous'
                },
                email: {
                    required: true,
                    pattern: 'email',
                    msg: 'Please enter a valid email address'
                },
                message: {
                    required: true,
                    msg: 'Please write a message, or tell a joke!'
                }
            }
        });
    });