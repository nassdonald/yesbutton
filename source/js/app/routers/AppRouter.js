define(['marionette', 'config/vent'], function(Marionette, vent) {
    return Marionette.AppRouter.extend({
        //"index" must be a method in AppRouter's controller
        appRoutes: {
            "": "index",
            "projects": "projects",
            "projects/:id(/)": "project",
            "about(/)": "about",
            "contact(/)": "contact",
            "*notFound": 'notFound' // Must be last route
        },
        initialize: function() {
        	this.listenTo(vent, 'stack:show', function(model) {
                this.navigate(model.get('route'));
            });
            this.listenTo(vent, 'stacks:visible', function() {
                this.navigate('/projects');
            });
        }
    });
});