define(['jquery', 'backbone.validation', 'velocity', 'views/BasePageView', 'config/vent', 'views/FooterView', 'text!templates/contact.html'],
    function ($, Validation, Velocity, BasePageView, vent, FooterView, contactTemplate) {

        return BasePageView.extend({
        	tagName: 'article',
            id: 'contact',
            className: 'inverse',
            sectionId: 'contact',
            inverse: true,
            template: _.template(contactTemplate),
            ui: {
                field: '.form-field',
                name: 'input.name',
                email: 'input.email',
                message: 'textarea.message',
                submit: '.submit',
                formMessages: '.form-messages'
            },
            events: {
                'click .logo': function(ev) {
                    ev.preventDefault();
                    vent.trigger('intro:reset');
                },
                'keyup @ui.field': function(ev) {
                    var field = $(ev.currentTarget).attr('name'),
                        error = this.model.preValidate(field, $(ev.currentTarget).val());

                    if (!this.validated) {
                        return;
                    }

                    if (error) {
                        this.onInvalid(this, field, error);
                    } else {
                        this.onValid(this, field);
                    }
                },
                'change @ui.field': function(ev) {
                    var field = $(ev.currentTarget).attr('name');
                    this.model.set(field, this.ui[field].val());
                },
            	'submit #contact-form': function(ev) {
                    var that = this,
                        name = this.model.get('name'),
                        email = this.model.get('email'),
                        message = this.model.get('message');

                    ev.preventDefault();

                    if (!this.model.isValid(true)) {

                        // Weirdness: blur then focus for shake animation to (re)apply
                        this.$el.find('.invalid')
                            .removeClass('shake-horizontal')
                            .blur().first().focus();
                        this.$el.find('.invalid')
                            .addClass('shake-horizontal')

                    } else {

                        this.ui.submit.addClass('submitting');

                        $.ajax({
                            type: 'POST',
                            url: 'php/contact.php',
                            data: 'name=' + name + '&email=' + email + '&message=' + message,
                            success: function(text) {
                                that.ui.submit.removeClass('submitting');
                                if (text === 'success') {
                                    that.formSuccess();
                                } else {
                                    that.formError(text);
                                }
                            },
                            error: function() {
                                that.ui.submit.removeClass('submitting');
                                that.formError();
                            }
                        });

                    }
            	}
            },
            modelEvents: {
                'validated': 'setValidated'
            },
            onShow: function() {
                var that = this;

            	// Super invocation!
                BasePageView.prototype.onShow.apply(this, arguments);

                Validation.bind(this, {
                       valid: that.onValid,
                       invalid: that.onInvalid
                   });
	        },
            onBeforeDestroy: function() {
                Backbone.Validation.unbind(this);

                // Super invocation!
                BasePageView.prototype.onBeforeDestroy.apply(this, arguments);
            },
            onValid: function(view, attr) {
                if (view.ui[attr].hasClass('valid')) {
                    return;
                }

                view.ui[attr]
                    .removeClass('invalid shake-horizontal')
                    .addClass('valid')
                    .next('.error-message');
            },
            onInvalid: function(view, attr, error) {
                if (view.ui[attr].hasClass('invalid')) {
                    return;
                }

                view.ui[attr]
                    .addClass('invalid')
                    .removeClass('valid')
                    .next('.error-message')
                    .text(error);
            },
            setValidated: function(valid, model) {
                this.validated = true;
            },
            formSuccess: function() {
                this.ui.formMessages
                    .addClass('success')
                    .text('Your message has been sent. Woo hoo!');

                this.ui.submit.addClass('success');

                this.showFormMessages();

                this.ui.field
                    .val('')
                    .removeClass('valid');

                this.model.clear().set(this.model.defaults);
            },
            formError: function(error) {
                this.ui.formMessages
                    .addClass('error')
                    .text(error || 'Uh oh! Something went wrong :-(');

                this.showFormMessages();
            },
            showFormMessages: function() {
                var that = this;
                this.ui.formMessages
                    .velocity('stop')
                    .velocity({
                        translateY: [0, '-100%'],
                        opacity: [1, 0.5]
                    }, {
                        display: 'block',
                        duration: 600,
                        easing: 'easeOutCubic'
                    })
                    .velocity('reverse', {
                        display: 'none',
                        delay: 3000,
                        complete: function() {
                            that.ui.submit.removeClass('success');
                        }
                    });
            }
        });
    });