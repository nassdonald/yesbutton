define( [ 'marionette', 'text!templates/footer.html'],
    function( Marionette, footerTemplate) {

        return Marionette.ItemView.extend( {
            className: 'footer-inner',
            template: _.template(footerTemplate),
            templateHelpers: function() {
                return {
                    year: new Date().getFullYear()
                }
            }
        });
    });