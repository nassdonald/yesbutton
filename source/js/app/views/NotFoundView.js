define([ 'views/BasePageView', 'config/vent', 'text!templates/not-found.html'],
    function ( BasePageView, vent, template) {

        return BasePageView.extend({
            id: 'notfound',
            sectionId: 'notfound',
            className: 'inverse',
            inverse: true,
            template: _.template(template),
            events: {
                'click .contact-link': function(ev) {
                    var app = require('App');
                    ev.preventDefault();
                    vent.trigger('contact:show');
                    app.appRouter.navigate('/contact');
                },
                'click .home-link': function(ev) {
                    ev.preventDefault();
                    vent.trigger('intro:reset');
                }
            },
        });
    });