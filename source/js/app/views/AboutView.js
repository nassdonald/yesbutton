define([ 'views/BasePageView', 'config/vent', 'text!templates/about.html'],
    function ( BasePageView, vent, aboutTemplate) {

        return BasePageView.extend({
            id: 'about',
            sectionId: 'about',
            inverse: true,
            template: _.template(aboutTemplate)
        });
    });