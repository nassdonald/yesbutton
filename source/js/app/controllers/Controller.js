define(['marionette', 'config/vent'],
    function (Marionette, vent) {
    return Marionette.Object.extend({
        initialize: function() {
            this.app = require('App');
            this.listenTo(vent, 'projects:show', this.projects);
            this.listenTo(vent, 'about:show', this.about);
            this.listenTo(vent, 'contact:show', this.contact);
            this.listenTo(vent, 'notfound:show', this.notFound);
        },

        //gets mapped to in AppRouter's appRoutes
        index: function () {
            this.app.rootLayout.onStackRoute("");
        },
        projects: function() {
            this.app.rootLayout.showStacks();
        },
        project: function(id) {
            this.app.rootLayout.onStackRoute('projects/' + id);
        },
        about: function() {
            this.app.rootLayout.showAbout();
        },
        contact: function () {
            this.app.rootLayout.showContact();
        },
        notFound: function() {
            this.app.rootLayout.showNotFound();
        }
    });
});