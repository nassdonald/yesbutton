({
    appDir: "../",
    baseUrl: "js/app",
    dir: "../../public",
    paths: {
        "init": 'config/Init',
        // "requireLib": "../libs/require",
        "jquery": "../libs/jquery-2.1.4.min",
        "underscore": "../libs/backbone.marionette/underscore-min",
        "backbone": "../libs/backbone.marionette/backbone-min",
        "marionette": "../libs/backbone.marionette/backbone.marionette.min",
        "velocity": "../libs/velocity.min",

        // // Plugins
        "text": "../libs/text",
        "backbone.wreqr": "../libs/backbone.marionette/backbone.wreqr",
        "backbone.babysitter": "../libs/backbone.marionette/backbone.babysitter",
        "backbone.validation": "../libs/backbone-validation-amd-min",
        "fastclick": "../libs/fastclick.min",
        "lazyload": "../libs/jquery.lazyload.min"
    },
    modules: [
        {
            name: 'init',
            // include: ['requireLib']
        }
    ],
    optimizeCss: "standard"
});